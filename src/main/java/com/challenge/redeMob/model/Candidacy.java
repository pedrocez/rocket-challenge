package com.challenge.redeMob.model;

import com.challenge.redeMob.enums.StatusCandidacy;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "candidacy", schema = "public")
public class Candidacy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "candidate_name")
    private String candidateName;

    @Column(name = "mother_name")
    private String motherName;

    @Column(name = "cpf")
    private String cpfCandidate;

    @Column(name = "birth_date")
    private LocalDate birthDateCandidate;

    @Column(name = "path_face_photo")
    private String pathFacePhoto;

    @Column(name = "path_identifier_photo")
    private String pathIdentifierPhoto;

    @Column(name = "path_proof_address")
    private String pathProofAddress;

    @Column(name = "status_candidacy")
    @Enumerated(EnumType.STRING)
    private StatusCandidacy statusCandidacy;
}