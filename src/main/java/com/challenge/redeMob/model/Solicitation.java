package com.challenge.redeMob.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "solicitation", schema = "public")
public class Solicitation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quantity_solicitation")
    private Integer quantitySolicitation;

    @Column(name = "cpf_candidate")
    private String cpfCandidate;

    @Column(name = "qtd_status_pending")
    private Integer qtdStatusPending;

    @Column(name = "qtd_status_recused")
    private Integer qtdStatusRecused;

    @Column(name = "qtd_status_approved")
    private Integer qtdStatusApproved;
}