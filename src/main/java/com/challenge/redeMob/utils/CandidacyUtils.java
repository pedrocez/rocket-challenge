package com.challenge.redeMob.utils;

import com.challenge.redeMob.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.Period;

@Slf4j
public class CandidacyUtils {

    public static void checkCandidateIsLegalAge(LocalDate ageCandidate) {
        log.info("Checking if the candidate is of legal age...");

        Period period = Period.between(ageCandidate, LocalDate.now());
        if (period.getYears() < 18) {
            log.error("The candidate is not of legal age.");
            throw new BusinessException("The candidate is not of legal age.");
        }
    }
}