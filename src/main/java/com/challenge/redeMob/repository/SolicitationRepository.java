package com.challenge.redeMob.repository;

import com.challenge.redeMob.model.Solicitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SolicitationRepository extends JpaRepository<Solicitation, Long> {

    Optional<Solicitation> findByCpfCandidate(String cpf);
}