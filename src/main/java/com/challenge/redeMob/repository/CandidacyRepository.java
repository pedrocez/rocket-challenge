package com.challenge.redeMob.repository;

import com.challenge.redeMob.enums.StatusCandidacy;
import com.challenge.redeMob.model.Candidacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidacyRepository extends JpaRepository<Candidacy, Long> {

    void deleteByCpfCandidateAndStatusCandidacy(String cpf, StatusCandidacy statusCandidacy);

    List<Candidacy> findByCpfCandidate(String cpf);

}