package com.challenge.redeMob.repository;

import com.challenge.redeMob.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    UserDetails findByLogin(String login);

    @Query(value = "SELECT u FROM User u WHERE u.login = :login")
    User findByUserLogin(String login);

    Optional<User> findByUserCpf(String cpf);
}