package com.challenge.redeMob.dto;

import com.challenge.redeMob.enums.UserRole;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Getter
@Data
public class RegisterUserDTO {

    private String login;
    private String password;
    private UserRole role;
}