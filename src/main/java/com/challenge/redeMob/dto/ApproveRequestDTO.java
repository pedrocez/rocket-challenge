package com.challenge.redeMob.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Data
public class ApproveRequestDTO {
    private boolean approved;
    private Long idCandidate;
}