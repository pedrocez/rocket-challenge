package com.challenge.redeMob.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class CandidateRequestDTO {

    private String nameCandidate;
    private String motherNameCandidate;
    private String cpfCandidate;

}