package com.challenge.redeMob.dto.filter;

import com.challenge.redeMob.enums.StatusCandidacy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CandidacyDTOFilter {
    private Long id;
    private String nameCandidate;
    private String motherNameCandidate;
    private String cpfCandidate;
    private LocalDate birthDateCandidate;
    private StatusCandidacy statusCandidacy;
    //private MultipartFile facePhoto;
    //private MultipartFile identifierPhoto;
    //private MultipartFile proofAddressPhoto;
}