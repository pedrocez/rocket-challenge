package com.challenge.redeMob.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

@Data
public class CandidacyDTO {

    private String nameCandidate;
    private String motherNameCandidate;
    private String cpfCandidate;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate birthDateCandidate;
    private String passwordCandidate;
    private MultipartFile facePhoto;
    private MultipartFile identifierPhoto;
    private MultipartFile proofAddressPhoto;
}