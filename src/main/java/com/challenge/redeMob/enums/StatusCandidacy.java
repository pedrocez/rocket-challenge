package com.challenge.redeMob.enums;

public enum StatusCandidacy {
    APPROVED,
    PENDING,
    RECUSED
}