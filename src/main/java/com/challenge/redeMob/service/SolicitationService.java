package com.challenge.redeMob.service;

import com.challenge.redeMob.exception.BusinessException;
import com.challenge.redeMob.model.Solicitation;
import com.challenge.redeMob.repository.SolicitationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class SolicitationService {

    @Autowired
    private SolicitationRepository solicitationRepository;

    public void updateQuantityCandidacy(Solicitation solicitation) {
        solicitationRepository.save(Solicitation.builder()
                .id(solicitation.getId())
                .quantitySolicitation(solicitation.getQuantitySolicitation() + 1)
                .build());
    }

    public void saveSolicitation(String cpfCandidate) {
        log.info("Creating register candidacy solicitation...");
        try {

            Optional<Solicitation> solicitation = solicitationRepository.findByCpfCandidate(cpfCandidate);
            if (solicitation.isPresent()) {
                updateQuantityCandidacy(solicitation.get());
            } else {

                Solicitation solicitationToSave = Solicitation.builder()
                        .cpfCandidate(cpfCandidate)
                        .quantitySolicitation(1)
                        .build();

                solicitationRepository.save(solicitationToSave);
            }

        } catch (BusinessException businessException) {
            log.error("Error creating candidacy solicitation.");
            throw new BusinessException("Error creating candidacy solicitation.");

        }
    }
}