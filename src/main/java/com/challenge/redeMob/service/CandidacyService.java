package com.challenge.redeMob.service;

import com.challenge.redeMob.dto.ApproveRequestDTO;
import com.challenge.redeMob.dto.CandidacyDTO;
import com.challenge.redeMob.dto.filter.CandidacyDTOFilter;
import com.challenge.redeMob.enums.StatusCandidacy;
import com.challenge.redeMob.exception.BusinessException;
import com.challenge.redeMob.model.Candidacy;
import com.challenge.redeMob.repository.CandidacyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.challenge.redeMob.mapper.CandidacyMapper.toDTOFilter;
import static com.challenge.redeMob.mapper.CandidacyMapper.toEntity;
import static com.challenge.redeMob.service.ImageService.uploadImage;

@Service
@Slf4j
public class CandidacyService {

    @Autowired
    private CandidacyRepository candidacyRepository;

    @Autowired
    private SolicitationService solicitationService;

    @Autowired
    private UserService userService;

    public void saveCandidacy(CandidacyDTO candidateDTO, Long userId) {
        log.info("Saving candidacy...");
        try {
            String pathFacePhoto = uploadImage(candidateDTO.getFacePhoto());
            String pathIdentifierPhoto = uploadImage(candidateDTO.getIdentifierPhoto());
            String pathProofAddressPhoto = uploadImage(candidateDTO.getProofAddressPhoto());

            Candidacy candidateToBeSave = toEntity(candidateDTO, pathFacePhoto, pathIdentifierPhoto,
                    pathProofAddressPhoto, userId);

            candidacyRepository.save(candidateToBeSave);
            log.info("Candidacy saved.");

        } catch (BusinessException businessException) {
            log.error("Error saving candidacy.");
            throw new BusinessException("Error saving candidacy.");
        }
    }

    public void checkCandidacy(CandidacyDTO candidacyDTO) {
        log.info("Checking candidacy exists...");

        List<Candidacy> candidacyList = candidacyRepository.findByCpfCandidate(candidacyDTO.getCpfCandidate());
        if (!candidacyList.isEmpty()) {
            checkCandidateCandidacies(candidacyList);
        }
    }

    public List<CandidacyDTOFilter> listAllCandidacy() {
        log.info("Finding all candidacies...");
        List<Candidacy> candidacyList = candidacyRepository.findAll();
        return getCandidacyDTOFilters(candidacyList);
    }

    private static List<CandidacyDTOFilter> getCandidacyDTOFilters(List<Candidacy> candidacyList) {
        List<CandidacyDTOFilter> candidacyDTOFilterList = new ArrayList<>();
        for (Candidacy candidacy : candidacyList) {
            candidacyDTOFilterList.add(toDTOFilter(candidacy));
        }
        return candidacyDTOFilterList;
    }

    public List<CandidacyDTOFilter> listAllCandidacyOfCandidate(String cpf) {
        List<Candidacy> listCandidacy = candidacyRepository.findByCpfCandidate(cpf);

        return getCandidacyDTOFilters(listCandidacy);
    }

    private void checkCandidateCandidacies(List<Candidacy> candidacyLlist) {
        log.info("Validating status of candidacies...");
        List<StatusCandidacy> statusCandidacies = new ArrayList<>();
        int quantityStatusRecused = 0;

        for (Candidacy candidacy : candidacyLlist) {
            statusCandidacies.add(candidacy.getStatusCandidacy());
        }

        for (StatusCandidacy status : statusCandidacies) {

            switch (status) {
                case APPROVED -> throw new BusinessException("The Candidate has already been approved." +
                        "That's why is not able to make new candidacy request.");
                case RECUSED -> quantityStatusRecused++;
            }
        }

        if (quantityStatusRecused == 2) {
            throw new BusinessException("Candidate is not able to make new candidacy request.");
        }
    }

    @Transactional
    public void approveCandidate(ApproveRequestDTO approveRequestDTO) {

        Optional<Candidacy> candidacy = candidacyRepository.findById(approveRequestDTO.getIdCandidate());
        boolean present = candidacy.isPresent();

        if (present) {
            if (approveRequestDTO.isApproved()) {
                String cpf = candidacy.get().getCpfCandidate();
                candidacyRepository.save(buildCandidacyToSave(candidacy, StatusCandidacy.APPROVED));
                candidacyRepository.deleteByCpfCandidateAndStatusCandidacy(cpf, StatusCandidacy.PENDING);

            } else {
                candidacyRepository.save(buildCandidacyToSave(candidacy, StatusCandidacy.RECUSED));
            }
        }
    }

    private Candidacy buildCandidacyToSave(Optional<Candidacy> candidacy, StatusCandidacy statusCandidacy) {
        return Candidacy.builder()
                .id(candidacy.get().getId())
                .candidateName(candidacy.get().getCandidateName())
                .cpfCandidate(candidacy.get().getCpfCandidate())
                .motherName(candidacy.get().getMotherName())
                .birthDateCandidate(candidacy.get().getBirthDateCandidate())
                .statusCandidacy(statusCandidacy)
                .pathFacePhoto(candidacy.get().getPathFacePhoto())
                .pathProofAddress(candidacy.get().getPathProofAddress())
                .pathIdentifierPhoto(candidacy.get().getPathIdentifierPhoto())
                .build();
    }
}