package com.challenge.redeMob.service;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Data
@Slf4j
public class ImageService {

    public static String uploadImage(MultipartFile file) {
        log.info("Uploading image: '{}' ", file.getOriginalFilename());
        try {

            Path uploadPath = Path.of("C:/Users/Win/Documents");
            Path filePath = uploadPath.resolve(Objects.requireNonNull(file.getOriginalFilename()));
            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
            log.info("image uploaded.");
            return filePath.toString();
        } catch (IOException exception) {
            log.error("Error uploading image: {}", file.getOriginalFilename());
            throw new RuntimeException("Error uploading image: " + file.getOriginalFilename());
        }
    }
}