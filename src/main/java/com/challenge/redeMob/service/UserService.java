package com.challenge.redeMob.service;

import com.challenge.redeMob.dto.CandidacyDTO;
import com.challenge.redeMob.enums.UserRole;
import com.challenge.redeMob.model.User;
import com.challenge.redeMob.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.challenge.redeMob.mapper.UserMapper.candidacyDTOtoRegisterUserDTO;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    public User createUser(CandidacyDTO candidacyDTO, UserRole role) {
        Optional<User> user = userRepository.findByUserCpf(candidacyDTO.getCpfCandidate());
        if (user.isEmpty()) {

            log.info("Creating user for candidate...");

            var registerUserDTO = candidacyDTOtoRegisterUserDTO(candidacyDTO, role);
            if (this.userRepository.findByLogin(registerUserDTO.getLogin()) != null) {
                log.info("User already exists.");
            }

            //String encryptedPassword = new BCryptPasswordEncoder().encode(registerUserDTO.getPassword());
            User newUser = new User(registerUserDTO.getLogin(), passwordEncoder.encode(registerUserDTO.getPassword()), registerUserDTO.getRole(), candidacyDTO.getCpfCandidate());
            return userRepository.save(newUser);
        }
        return user.get();
    }

    public User authenticate(String login, String password) {

        User user = userRepository.findByUserLogin(login);

        if (!passwordEncoder.matches(password, user.getPassword())) {
            log.error("Senhas inválidas.");
        }
        ;

        return user;
    }
}