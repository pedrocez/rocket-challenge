package com.challenge.redeMob.controller;

import com.challenge.redeMob.dto.ApproveRequestDTO;
import com.challenge.redeMob.dto.CandidacyDTO;
import com.challenge.redeMob.dto.CandidateRequestDTO;
import com.challenge.redeMob.dto.filter.CandidacyDTOFilter;
import com.challenge.redeMob.enums.UserRole;
import com.challenge.redeMob.exception.BusinessException;
import com.challenge.redeMob.model.User;
import com.challenge.redeMob.service.CandidacyService;
import com.challenge.redeMob.service.SolicitationService;
import com.challenge.redeMob.service.UserService;
import com.challenge.redeMob.utils.ResponseMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.challenge.redeMob.utils.CandidacyUtils.checkCandidateIsLegalAge;

@Slf4j
@RestController
public class CandidacyController {

    @Autowired
    private CandidacyService candidacyService;

    @Autowired
    private SolicitationService solicitationService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/candidacy", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<ResponseMessage> registerCandidacy(@ModelAttribute CandidacyDTO candidacyDTO, UserRole role
    ) {
        try {
            processCandidacy(candidacyDTO, role);
            return new ResponseEntity<>(new ResponseMessage("Candidacy registered successfully.",
                    true), HttpStatus.CREATED);
        } catch (BusinessException businessException) {
            return new ResponseEntity<>(new ResponseMessage(businessException.getMessage(),
                    false), HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/admin/candidacies", method = RequestMethod.GET)
    public ResponseEntity<List<CandidacyDTOFilter>> getAllCandidacy() {
        List<CandidacyDTOFilter> candidacyDTOFilterList = candidacyService.listAllCandidacy();
        return ResponseEntity.ok(candidacyDTOFilterList);

    }

    @RequestMapping(value = "/candidate/candidacies", method = RequestMethod.GET)
    public ResponseEntity<List<CandidacyDTOFilter>> getCandidaciesOfCandidate(@RequestBody CandidateRequestDTO candidateRequestDTO) {
        List<CandidacyDTOFilter> candidacyDTOFilterList = candidacyService.listAllCandidacyOfCandidate(candidateRequestDTO.getCpfCandidate());
        return ResponseEntity.ok(candidacyDTOFilterList);

    }

    @RequestMapping(value = "/admin/approve-candidate", method = RequestMethod.POST)
    public ResponseEntity approveCandidate(@RequestBody ApproveRequestDTO approveRequestDTO) {
        candidacyService.approveCandidate(approveRequestDTO);
        return ResponseEntity.ok().build();

    }

    private void processCandidacy(CandidacyDTO candidacyDTO, UserRole role) {
        log.info("Processing candidacy register...");
        checkCandidateIsLegalAge(candidacyDTO.getBirthDateCandidate());
        User user = userService.createUser(candidacyDTO, role);
        candidacyService.checkCandidacy(candidacyDTO);
        candidacyService.saveCandidacy(candidacyDTO, user.getId());
        solicitationService.saveSolicitation(candidacyDTO.getCpfCandidate());
    }
}