package com.challenge.redeMob.mapper;

import com.challenge.redeMob.dto.CandidacyDTO;
import com.challenge.redeMob.dto.RegisterUserDTO;
import com.challenge.redeMob.enums.UserRole;

public class UserMapper {

    public static RegisterUserDTO candidacyDTOtoRegisterUserDTO(CandidacyDTO candidacyDTO, UserRole role) {
        return RegisterUserDTO.builder()
                .login(candidacyDTO.getNameCandidate())
                .password(candidacyDTO.getPasswordCandidate())
                .role(role)
                .build();
    }
}