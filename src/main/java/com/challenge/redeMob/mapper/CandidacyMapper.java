package com.challenge.redeMob.mapper;

import com.challenge.redeMob.dto.CandidacyDTO;
import com.challenge.redeMob.dto.filter.CandidacyDTOFilter;
import com.challenge.redeMob.enums.StatusCandidacy;
import com.challenge.redeMob.model.Candidacy;

public class CandidacyMapper {

    public static Candidacy toEntity(CandidacyDTO candidacyDTO, String pathFacePhoto, String pathIdentifierPhoto,
                                     String pathProofAddressPhoto, Long userId) {
        return Candidacy.builder()
                .candidateName(candidacyDTO.getNameCandidate())
                .cpfCandidate(candidacyDTO.getCpfCandidate())
                .birthDateCandidate(candidacyDTO.getBirthDateCandidate())
                .motherName(candidacyDTO.getMotherNameCandidate())
                .pathFacePhoto(pathFacePhoto)
                .pathProofAddress(pathProofAddressPhoto)
                .pathIdentifierPhoto(pathIdentifierPhoto)
                .statusCandidacy(StatusCandidacy.PENDING)
                .build();
    }

    public static CandidacyDTOFilter toDTOFilter(Candidacy candidacy) {
        return CandidacyDTOFilter.builder()
                .id(candidacy.getId())
                .nameCandidate(candidacy.getCandidateName())
                .cpfCandidate(candidacy.getCpfCandidate())
                .motherNameCandidate(candidacy.getMotherName())
                .birthDateCandidate(candidacy.getBirthDateCandidate())
                .statusCandidacy(candidacy.getStatusCandidacy())
                //.facePhoto()
                //identifierPhoto()
                //.proofAddressPhoto()
                .build();
    }
}