package com.challenge.redeMob.exception;

import java.io.IOException;

public class UploadFileException extends IOException {

    public UploadFileException(String msg) {
        super(msg);
    }

}