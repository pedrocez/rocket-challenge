# Challenge rocket

Consiste em uma parte da entrega do desafio solicitado pela equipe técnica da empresa RedeMob Consórcios.

## Pré-requisitos para execução local
- Java 17: https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html
- Banco de dados PostgreSQL 
- Spring Boot
- Maven 
- Máquina virtual do Docker para criação do container (Pode ser baixada pelo Docker Desktop) : https://www.docker.com/products/docker-desktop/
- Postman

## Banco de dados

Note que no diretório raíz do projeto existe uma pasta de nome "rocket-db".
Acesse o diretório via terminal com o comando:

```bash
cd .\rocket-db\
```

Dentro do diretório execute:

```bash
docker compose up
```

Com esse comando você terá um banco de dados Postgres sendo executado em um container docker em sua máquina.

## Postman

Dentro da raíz do projetoe existe um arquivo Json para import da collection no postman. 

Para importar:
- Abra o postman
- clique em import no canto superior esquerdo
- Em seguida arraste o arquivo Json para o modal e o postman importará a collection

OBS: O endpoint de Login ainda não está pronto devido a uns problemas na autenticação.

Para build e execução do projeto, escolha uma IDE de sua preferência.

